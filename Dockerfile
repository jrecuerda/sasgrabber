FROM jrecuerda/gcc-conan

# Copy sources
COPY CMakeLists.txt conanfile.py root/
COPY src root/src

RUN mkdir root/build 

# Build
WORKDIR root/build

RUN conan install .. -s compiler=gcc -s compiler.libcxx=libstdc++11 -s build_type=Release --build missing \
    && cmake .. \
    && make -j$(nproc) install \ 
	 && find /root/.conan/data -type f  -regex '.*\/lib\/.*\.so.*' -exec cp {} ./bin \;  \
	 && touch /etc/grabber.conf


ENV LD_LIBRARY_PATH /build/bin:${LD_LIBRARY_PATH}

ENTRYPOINT ["./bin/Grabber"]
CMD ["-config-file", "/etc/grabber.conf"]
