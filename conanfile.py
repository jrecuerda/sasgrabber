from conans import ConanFile, CMake

class Grabber(ConanFile):
   settings = "os", "compiler", "build_type", "arch"
   requires = "Poco/1.9.0@jrecuerda/stable"
   generators = "cmake", "gcc", "txt"
   default_options = "Poco:shared=False", "Poco:cxx_14=True", "Poco:enable_data_mysql=True", "OpenSSL:shared=False"


   def imports(self):
      self.copy("*.dll", dst="bin", src="bin") # From bin to bin
      self.copy("*.dylib*", dst="bin", src="lib") # From lib to bin

#   def build(self):
#      cmake = CMake(self)
#      cmake.configure()
#      cmake.build()
