#include "ConfigurationParams.h"
#include "ContractsFeeder.h"
#include "DBPersister.h"
#include "ExecutorSAS.h"
#include "FilePersister.h"
#include "RequestorSAS.h"

#include "Poco/AutoPtr.h"
#include "Poco/ConsoleChannel.h"
#include "Poco/Data/MySQL/Connector.h"
#include "Poco/Environment.h"
#include "Poco/Exception.h"
#include "Poco/FileChannel.h"
#include "Poco/FormattingChannel.h"
#include "Poco/LogStream.h"
#include "Poco/PatternFormatter.h"
#include "Poco/SplitterChannel.h"
#include "Poco/Util/Application.h"
#include "Poco/Util/OptionSet.h"
#include "Poco/Util/XMLConfiguration.h"

#include <future>
#include <iostream>
#include <memory>
#include <sstream>

using namespace Poco;
using Poco::Logger;
using Poco::LogStream;
using namespace Poco::Util;

class App : public Application
{
protected:
   void initialize(Application& application) override
   {
      Application::initialize(application);
      setupLogger();
      Poco::Data::MySQL::Connector::registerConnector();
   }

   void uninitialize() override
   {
      Application::uninitialize();
   }

   void reinitialize(Application& application) override
   {
      Application::reinitialize(application);
   }

   void defineOptions(OptionSet& optionSet) override
   {
      Application::defineOptions(optionSet);

      optionSet.addOption(
         Option("config-file", "config-file")
            .required(true)
            .repeatable(false)
            .argument("path")
            .callback(OptionCallback<App>(this, &App::initConfiguration)));
   }

   void initConfiguration(const std::string& name, const std::string& value)
   {
      poco_configuration.reset(new XMLConfiguration(value));
   }

   int main(const std::vector<std::string>& args) override
   {
      int result = EXIT_SUCCESS;
      try
      {
         LogStream lstr("Grabber");
         if(!poco_configuration)
         {
            return EXIT_FAILURE;
         }

         lstr.information() << "Entering application!"
                            << "\n";
         execute(ConfigurationParams(poco_configuration.get()));
      }
      catch(Exception&)
      {
         result = EXIT_FAILURE;
      }

      return result;
   }

private:
   AutoPtr<AbstractConfiguration> poco_configuration;

   void execute(ConfigurationParams&& configuration)
   {
      std::shared_ptr<Persister> persister;
      if(configuration.filePersister)
      {
         persister = std::make_shared<FilePersister>(configuration.outputFile);
      }
      else
      {
         persister = std::make_shared<DBPersister>(configuration);
      }
      ContractsFeeder contractsFeeder(configuration);
      ExecutorSAS     executor(configuration, contractsFeeder, *persister);
      executor.run();

      //    std::vector<std::shared_ptr<ExecutorSAS>>
      //    workers(configuration.numWorkers);
      //    std::generate(std::begin(workers), std::end(workers),
      //                        [&]
      //                        {
      //                            return
      //                            std::make_shared<ExecutorSAS>(configuration,contractsFeeder,persister);
      //                        });

      //    std::vector<std::future<void>> futures;
      //    std::transform(std::begin(workers), std::end(workers),
      //    std::back_inserter(futures),
      //                        [](auto& currWorker)
      //                        {
      //                            return std::async(std::launch::async,
      //                            [&currWorker]{currWorker->run();});
      //                        });

      //    std::for_each(std::begin(futures), std::end(futures),[](auto&
      //    fut){fut.get();});
   }

   void setupLogger()
   {
      AutoPtr<ConsoleChannel> pCons(new ConsoleChannel);
      AutoPtr<FileChannel>    pFile(new FileChannel());
      pFile->setProperty("path", "output.log");
      pFile->setProperty("rotation", "2 M");
      pFile->setProperty("archive", "timestamp");
      AutoPtr<SplitterChannel> pSplitter(new SplitterChannel);
      pSplitter->addChannel(pCons);
      pSplitter->addChannel(pFile);

      AutoPtr<PatternFormatter> pPF(new PatternFormatter);
      pPF->setProperty("pattern", "%Y-%m-%d %H:%M:%S %p: %t");
      AutoPtr<FormattingChannel> pFC(new FormattingChannel(pPF, pSplitter));
      Logger::root().setChannel(pFC);
      if(Poco::Environment::get("GRABBER_LOG_LEVEL", "default") == "DEBUG")
      {
         Logger::root().setLevel(Poco::Message::PRIO_TRACE);
         auto& logger = Logger::get("Grabber");
         logger.trace("LOG LEVEL SET TO TRACE");
      }
   }
};

POCO_APP_MAIN(App)
