#include "ParserSAS.h"

#include "Poco/LogStream.h"
#include "Poco/Logger.h"

#include <iostream>
#include <stdlib.h>

namespace
{
std::regex rgx("NOTA\\s*-->\\s*(\\d+\\.?\\d*).*FECHA\\s*-->\\s*(\\d+)/(\\d+)/(\\d+)");
}

bool ParserSAS::operator()(Contract& contract, const std::string& response)
{
   std::smatch matches;

   if(std::regex_search(response, matches, rgx))
   {
      if(matches.size() != 5)
      {
         Poco::LogStream logger("Grabber.ParserSAS");
         logger.warning() << "Invalid number of matches " << matches.size()
                          << ";Role: " << contract.getRole()
                          << ";System: " << contract.getSystem()
                          << ";Unit: " << contract.getUnit() << "\n";
         return false;
      }

      contract.setMark(atof(matches[1].str().c_str()));
      contract.setDate(Contract::Date(
         atoi(matches[2].str().c_str()),
         atoi(matches[3].str().c_str()),
         atoi(matches[4].str().c_str())));
   }
   else
   {
      if(!nobodyFound(response))
      {
         Poco::LogStream logger("Grabber.ExecutorSAS");
         logger.warning() << "NOT MATCHED: " + getLabelNotPared(response)
                          << ";Role: " << contract.getRole()
                          << ";System: " << contract.getSystem()
                          << ";Unit: " << contract.getUnit() << "\n";
         return false;
      }
   }

   auto&              logger = Poco::Logger::get("Grabber.ParserSAS");
   std::ostringstream ostream;
   ostream << "Parsed->"
           << "\tUnit: " << contract.getUnit() << "\tDuration: " << contract.getDuration()
           << "\tTypeOfPersonal: " << contract.getTypeOfPersonal()
           << "\tRole: " << contract.getRole() << "\tSystem: " << contract.getSystem()
           << "\tMark: " << contract.getMark() << "\n";

   logger.trace(ostream.str());

   return true;
}

bool ParserSAS::nobodyFound(const std::string& response) const
{
   std::regex  rgx("No se ha contratado a nadie");
   std::smatch matches;

   return std::regex_search(response, matches, rgx);
}


std::string ParserSAS::getLabelNotPared(const std::string& response) const
{
   std::regex  rgx("<label>(.*NOTA.*)<\\/label>");
   std::smatch matches;

   if(std::regex_search(response, matches, rgx))
   {
      return matches[1].str();
   }
   return std::string();
}
