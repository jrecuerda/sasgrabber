#include "ContractsFeeder.h"
#include "HTTPCombinations.h"

#include "Poco/Data/MySQL/Connector.h"
#include "Poco/Data/MySQL/MySQLException.h"
#include "Poco/Data/SessionFactory.h"
#include "Poco/LogStream.h"

#include <algorithm>
#include <random>

using namespace Poco::Data::Keywords;
using Poco::Data::Session;
using Poco::Data::Statement;

namespace
{
constexpr int LogFrequency = 100;
}

ContractsFeeder::ContractsFeeder(ConfigurationParams& configurationParams)
   : _configuration(configurationParams)
   , _currIt(std::end(_contracts))
   , _combinationsParsed(0)
   , _contractsParsed(0)
   , _vacanciesParsed(0)
   , _requestsDone(1)
   , _requestsLogCicles(0)
   , _databaseName(configurationParams.sqlConfig.databaseName)
{
}

void ContractsFeeder::initSession()
{
   if(!_db_session)
   {
      std::string dbConnString;
      dbConnString = "host=" + _configuration.sqlConfig.host;
      dbConnString += ";user=" + _configuration.sqlConfig.user;
      dbConnString += ";password=" + _configuration.sqlConfig.password;
      dbConnString += ";compress=true;auto-reconnect=true";

      _db_session =
         std::make_shared<Session>(Poco::Data::SessionFactory::instance().create(
            Poco::Data::MySQL::Connector::KEY, dbConnString));
   }
}

ContractsFeeder::~ContractsFeeder() {}

void ContractsFeeder::shuffle()
{
   std::random_device rd;
   std::mt19937       g(rd());

   std::shuffle(std::begin(_contracts), std::end(_contracts), g);
}

Contract& ContractsFeeder::operator()()
{
   std::lock_guard<std::mutex> lock(_contractsMtx);
   if(_currIt == std::end(_contracts))
   {
      recreateContracts();
   }
   if((_requestsDone) % LogFrequency == 0)
   {
      _requestsDone = 1;
      _requestsLogCicles++;
      Poco::LogStream logger("Grabber.ContractsFeeder");
      logger.information() << "# Requests done = " << (_requestsLogCicles * LogFrequency)
                           << "\n";
   }
   _requestsDone++;
   return *_currIt++;
}

void ContractsFeeder::recreateContracts()
{
   fillContracts();
   shuffle();
   _currIt = std::begin(_contracts);
   if(_currIt == std::end(_contracts))
   {
      Poco::LogStream logger("Grabber.ContractsFeeder");
      logger.fatal() << "Empty contracts list after filling"
                     << "\n";
      throw;
   }
}

void ContractsFeeder::fillContracts()
{
   _contracts.clear();

   Poco::LogStream logger("Grabber.ContractsFeeder");
   logger.information() << "Filling contracts\n";
   try
   {
      initSession();
      if(_combinationsParsed < 1 && _configuration.beginWithCombinations)
      {
         fillWithCombinations();
         _combinationsParsed++;
      }
      else if(_contractsParsed < _configuration.contractsExecPerVacancies)
      {
         _contractsParsed++;
         fillWithContracts();
      }
      else
      {
         _contractsParsed = 0;
         fillWithVacancies();
      }
   }
   catch(Poco::Data::ConnectionFailedException& e)
   {
      Poco::LogStream logger("Grabber.ContractsFeeder");
      logger.error() << std::string("Connection Exception: ") +
                           std::string(e.displayText())
                     << "\n";
   }
   catch(Poco::Data::MySQL::MySQLException& e)
   {
      Poco::LogStream logger("Grabber.ContractsFeeder");
      logger.error() << std::string("SQL error: ") + std::string(e.displayText()) << "\n";
   }
}

void ContractsFeeder::fillWithContracts()
{
   Statement select(*_db_session);
   int       typeOfPersonal;
   int       duration;
   int       system;
   int       unit;
   int       role;
   select << "select type_of_personal_id,"
             "unit_id,"
             "role_id,"
             "duration_id,"
             "system_id "
             "from " +
                _databaseName +
                ".contracts"
                " group by type_of_personal_id, unit_id, "
                "role_id, duration_id, system_id",
      into(typeOfPersonal), into(unit), into(role), into(duration), into(system),
      range(0, 1);

   while(!select.done())
   {
      select.execute();
      _contracts.emplace_back(
         unit,
         role,
         Contract::getTypeOfPersonal(typeOfPersonal),
         Contract::getDuration(duration),
         Contract::getSystem(system));
   }
}

void ContractsFeeder::fillWithVacancies()
{
   Statement select(*_db_session);
   int       typeOfPersonal;
   int       duration;
   int       system;
   int       unit;
   int       role;
   select << "select type_of_personal_id,"
             "unit_id,"
             "role_id,"
             "duration_id,"
             "system_id "
             "from " +
                _databaseName +
                ".vacancies"
                " group by type_of_personal_id, unit_id, "
                "role_id, duration_id, system_id",
      into(typeOfPersonal), into(unit), into(role), into(duration), into(system),
      range(0, 1);

   while(!select.done())
   {
      select.execute();
      _contracts.emplace_back(
         unit,
         role,
         Contract::getTypeOfPersonal(typeOfPersonal),
         Contract::getDuration(duration),
         Contract::getSystem(system));
   }
}

void ContractsFeeder::fillWithCombinations()
{

   HTTPCombinations combinations;
   const auto&      combinationsVect = combinations.getFormCombinations();
   for(auto& combination : combinationsVect)
   {
      auto contractsForCombination = getContractsFromCombination(combination);
      std::move(
         std::begin(contractsForCombination),
         std::end(contractsForCombination),
         std::back_inserter(_contracts));
   }
}

std::vector<Contract>
   ContractsFeeder::getContractsFromCombination(const HTTPFormCombination& combination)
{
   std::vector<Contract> contracts;
   if(combination.getTypeOfPersonal() != 'C')
   {
      contracts.emplace_back(
         combination.getUnit(),
         combination.getRole(),
         combination.getTypeOfPersonal(),
         'S',
         'L');
      contracts.emplace_back(
         combination.getUnit(),
         combination.getRole(),
         combination.getTypeOfPersonal(),
         'S',
         'R');
      contracts.emplace_back(
         combination.getUnit(),
         combination.getRole(),
         combination.getTypeOfPersonal(),
         'S',
         'I');
      contracts.emplace_back(
         combination.getUnit(),
         combination.getRole(),
         combination.getTypeOfPersonal(),
         'V',
         'L');
      contracts.emplace_back(
         combination.getUnit(),
         combination.getRole(),
         combination.getTypeOfPersonal(),
         'V',
         'R');
      contracts.emplace_back(
         combination.getUnit(),
         combination.getRole(),
         combination.getTypeOfPersonal(),
         'V',
         'I');
   }
   else
   {
      contracts.emplace_back(
         combination.getUnit(),
         combination.getRole(),
         combination.getTypeOfPersonal(),
         'S',
         'L');
      contracts.emplace_back(
         combination.getUnit(),
         combination.getRole(),
         combination.getTypeOfPersonal(),
         'S',
         'R');
      contracts.emplace_back(
         combination.getUnit(),
         combination.getRole(),
         combination.getTypeOfPersonal(),
         'V',
         'L');
      contracts.emplace_back(
         combination.getUnit(),
         combination.getRole(),
         combination.getTypeOfPersonal(),
         'V',
         'R');
   }
   return contracts;
}
