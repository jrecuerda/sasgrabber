#include "ConfigurationParams.h"

#include "Poco/Environment.h"
#include "Poco/Util/AbstractConfiguration.h"

using Poco::Util::AbstractConfiguration;

SQLConfiguration::SQLConfiguration(AbstractConfiguration* configuration)
{
   outputFile   = configuration->getString("OutputFile");
   user         = configuration->getString("SQLConfig[@id=user]");
   databaseName = configuration->getString("SQLConfig[@id=database_name]");
   if(Poco::Environment::has("GRABBER_DB_HOST"))
   {
      host = Poco::Environment::get("GRABBER_DB_HOST");
   }
   else
   {
      host = configuration->getString("SQLConfig[@id=host]");
   }

   if(Poco::Environment::has("GRABBER_DB_PASSWORD"))
   {
      password = Poco::Environment::get("GRABBER_DB_PASSWORD");
   }
   else
   {
      password = configuration->getString("SQLConfig[@id=host]");
   }
}

ConfigurationParams::ConfigurationParams(AbstractConfiguration* configuration)
   : sqlConfig(configuration)
{
   filePersister             = configuration->getBool("FilePersister", false);
   numWorkers                = configuration->getInt("NumWorkers", 1);
   inputputFile              = configuration->getString("InputFile", "");
   outputFile                = configuration->getString("OutputFile", "");
   url                       = configuration->getString("URL");
   beginWithCombinations     = configuration->getBool("BeginWithCombinations", false);
   contractsExecPerVacancies = configuration->getInt("ContractExecutionPerVacancies", 10);
}
