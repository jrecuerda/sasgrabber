#include "Contract.h"

#include "Poco/LogStream.h"

#include <algorithm>
#include <array>
#include <iostream>


namespace
{
constexpr std::array<std::pair<int, char>, 3> TypeOfPersonalMapping{
   std::make_pair(1, 'A'), std::make_pair(2, 'B'), std::make_pair(3, 'C')};

constexpr std::array<std::pair<int, char>, 2> DurationMapping{std::make_pair(1, 'S'),
                                                              std::make_pair(2, 'V')};

constexpr std::array<std::pair<int, char>, 3> SystemMapping{
   std::make_pair(1, 'L'), std::make_pair(2, 'R'), std::make_pair(3, 'I')};
} // namespace


char Contract::getTypeOfPersonal(int type)
{
   auto it = std::find_if(
      std::begin(TypeOfPersonalMapping),
      std::end(TypeOfPersonalMapping),
      [type](const auto& curr) { return curr.first == type; });
   if(it != std::end(TypeOfPersonalMapping))
   {
      return it->second;
   }
   else
   {
      Poco::LogStream logger("Grabber.Contract");
      logger.warning() << "TypeOfPersonal: '" << type << "' not valid.\n";

      return 'Y';
   }
}

int Contract::getTypeOfPersonal(char type)
{
   auto it = std::find_if(
      std::begin(TypeOfPersonalMapping),
      std::end(TypeOfPersonalMapping),
      [type](const auto& curr) { return curr.second == type; });
   if(it != std::end(TypeOfPersonalMapping))
   {
      return it->first;
   }
   else
   {
      Poco::LogStream logger("Grabber.Contract");
      logger.warning() << "TypeOfPersonal: '" << type << "' not valid.\n";
      return -1;
   }
}

char Contract::getDuration(int duration)
{

   auto it = std::find_if(
      std::begin(DurationMapping),
      std::end(DurationMapping),
      [duration](const auto& curr) { return curr.first == duration; });
   if(it != std::end(DurationMapping))
   {
      return it->second;
   }
   else
   {
      Poco::LogStream logger("Grabber.Contract");
      logger.warning() << "DurationMapping: '" << duration << "' not valid.\n";
      return -1;
   }
}

int Contract::getDuration(char duration)
{

   auto it = std::find_if(
      std::begin(DurationMapping),
      std::end(DurationMapping),
      [duration](const auto& curr) { return curr.second == duration; });
   if(it != std::end(DurationMapping))
   {
      return it->first;
   }
   else
   {
      Poco::LogStream logger("Grabber.Contract");
      logger.warning() << "DurationMapping: '" << duration << "' not valid.\n";
      return -1;
   }
}

char Contract::getSystem(int system)
{

   auto it = std::find_if(
      std::begin(SystemMapping), std::end(SystemMapping), [system](const auto& curr) {
         return curr.first == system;
      });
   if(it != std::end(SystemMapping))
   {
      return it->second;
   }
   else
   {
      Poco::LogStream logger("Grabber.Contract");
      logger.warning() << "SystemMapping: '" << system << "' not valid.\n";
      return 'Y';
   }
}

int Contract::getSystem(char system)
{

   auto it = std::find_if(
      std::begin(SystemMapping), std::end(SystemMapping), [system](const auto& curr) {
         return curr.second == system;
      });
   if(it != std::end(SystemMapping))
   {
      return it->first;
   }
   else
   {
      Poco::LogStream logger("Grabber.Contract");
      logger.warning() << "SystemMapping: '" << system << "' not valid.\n";
      return -1;
   }
}

Contract::Date::Date() : _day(-1), _month(-1), _year(-1) {}

Contract::Date::Date(int day, int month, int year) : _day(day), _month(month), _year(year)
{
}

int Contract::Date::getDay() const
{
   return _day;
}
void Contract::Date::setDay(int day)
{
   _day = day;
}

int Contract::Date::getMonth() const
{
   return _month;
}
void Contract::Date::setMonth(int month)
{
   _month = month;
}

int Contract::Date::getYear() const
{
   return _year;
}
void Contract::Date::setYear(int year)
{
   _year = year;
}

Contract::Contract(int unit, int role, char typeOfPersonal, char duration, char system)
   : _unit(unit)
   , _role(role)
   , _typeOfPersonal(typeOfPersonal)
   , _duration(duration)
   , _system(system)
   , _mark(-1)
{
}

int Contract::getUnit() const
{
   return _unit;
}

int Contract::getRole() const
{
   return _role;
}
char Contract::getTypeOfPersonal() const
{
   return _typeOfPersonal;
}
int Contract::getTypeOfPersonalId() const
{
   return Contract::getTypeOfPersonal(_typeOfPersonal);
}
char Contract::getDuration() const
{
   return _duration;
}
int Contract::getDurationId() const
{
   return Contract::getDuration(_duration);
}
char Contract::getSystem() const
{
   return _system;
}
int Contract::getSystemId() const
{
   return Contract::getSystem(_system);
}

double Contract::getMark() const
{
   return _mark;
}
void Contract::setMark(double mark)
{
   _mark = mark;
}

Contract::Date Contract::getDate() const
{
   return _date;
}

void Contract::setDate(Contract::Date date)
{
   _date = date;
}
