#include "ExecutorSAS.h"
#include "ConfigurationParams.h"
#include "ContractsFeeder.h"
#include "HTTPCombinations.h"
#include "Persister.h"

#include "Poco/Exception.h"
#include "Poco/LogStream.h"
#include "Poco/Net/HTMLForm.h"

#include <sstream>
#include <thread>

using Poco::Net::HTMLForm;

namespace
{
}

ExecutorSAS::ExecutorSAS(
   ConfigurationParams& configuration,
   ContractsFeeder&     contractsFeeder,
   Persister&           persister)
   : _stopExecution(false)
   , _contractsFeeder(contractsFeeder)
   , _persister(persister)
   , _requestor(RequestorSAS(configuration.url))
   , _parser()
   , _configurationParams(configuration)
{
}

ExecutorSAS::~ExecutorSAS() {}

void ExecutorSAS::stopExecution()
{
   _stopExecution.store(true);
}

void ExecutorSAS::execute(Contract& contract)
{
   if(!makeRequest(contract))
   {
      // Set failed value
      contract.setMark(-100);
   }
   _persister(contract);
}


void ExecutorSAS::createHTMLForm(const Contract& contract, Poco::Net::HTMLForm& form)
{
   std::ostringstream streamTypeOfPersonal;
   streamTypeOfPersonal << contract.getTypeOfPersonal();
   form.set("tipo_personal", streamTypeOfPersonal.str());
   form.set("centro", std::to_string(contract.getUnit()));
   form.set("combo_categoria", std::to_string(contract.getRole()));
   std::ostringstream streamDuration;
   streamDuration << contract.getDuration();
   form.set("t_vinculacion", streamDuration.str());
   std::ostringstream streamSystem;
   streamSystem << contract.getSystem();
   form.set("sistema_acceso", streamSystem.str());
}

bool ExecutorSAS::makeRequest(Contract& contract)
{
   HTMLForm form;
   createHTMLForm(contract, form);
   std::string responseStr;
   try
   {
      responseStr = _requestor.requestForm(form);
   }
   catch(Poco::TimeoutException& e)
   {
      Poco::LogStream logger("Grabber.ExecutorSAS");
      logger.error() << "Timeout:" + std::to_string(contract.getUnit()) + ";" +
                           std::to_string(contract.getRole())
                     << "\n";
      return false;
   }
   catch(Poco::Exception& e)
   {
      Poco::LogStream logger("Grabber.ExecutorSAS");
      logger.error() << "Unknown exception:" + std::string(e.what()) << "\n";
      return false;
   }

   return _parser(contract, responseStr);
}

void ExecutorSAS::run()
{
   while(!_stopExecution.load())
   {
      auto& contract = _contractsFeeder();
      execute(contract);
   }
}
