#include "RequestorSAS.h"

#include "Poco/LogStream.h"
#include "Poco/Net/Context.h"
#include "Poco/Net/HTMLForm.h"
#include "Poco/Net/HTTPMessage.h"
#include "Poco/Net/HTTPRequest.h"
#include "Poco/Net/HTTPResponse.h"
#include "Poco/Net/HTTPSClientSession.h"
#include "Poco/StreamCopier.h"
#include "Poco/URI.h"

#include <sstream>

using Poco::StreamCopier;
using Poco::URI;
using Poco::Net::Context;
using Poco::Net::HTMLForm;
using Poco::Net::HTTPRequest;
using Poco::Net::HTTPResponse;
using Poco::Net::HTTPSClientSession;

RequestorSAS::RequestorSAS(const std::string& uriStr)
{
   URI         uri(uriStr);
   std::string path(uri.getPathAndQuery());
   if(path.empty())
      path = "/";

   const Context::Ptr context = new Context(
      Context::CLIENT_USE,
      "",
      "",
      "",
      Context::VERIFY_NONE,
      9,
      false,
      "ALL:!ADH:!LOW:!EXP:!MD5:@STRENGTH");

   _session = std::make_shared<HTTPSClientSession>(uri.getHost(), uri.getPort(), context);

   _session->setTimeout(Poco::Timespan(20, 0));

   _request = std::make_shared<HTTPRequest>(
      HTTPRequest::HTTP_POST, path, Poco::Net::HTTPMessage::HTTP_1_1);
}

std::string RequestorSAS::requestForm(HTMLForm& form)
{
   std::string ret;
   form.prepareSubmit(*_request);
   form.write(_session->sendRequest(*_request));

   Poco::Net::HTTPResponse res;
   std::istream&           responseSteram = _session->receiveResponse(res);

   std::ostringstream streamResponseResult;
   streamResponseResult << res.getStatus() << " " << res.getReason();
   if(res.getStatus() != HTTPResponse::HTTP_OK)
   {
      Poco::LogStream logger("Grabber.RequestorSAS");
      logger.warning() << "Request result = " << res.getStatus() << " " << res.getReason()
                       << "\n";
   }
   StreamCopier::copyToString(responseSteram, ret);

   return ret;
}
