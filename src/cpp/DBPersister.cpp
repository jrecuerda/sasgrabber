#include "DBPersister.h"
#include "ConfigurationParams.h"
#include "Contract.h"

#include "Poco/Data/MySQL/Connector.h"
#include "Poco/Data/MySQL/MySQLException.h"
#include "Poco/Data/SessionFactory.h"
#include "Poco/LogStream.h"

using namespace Poco::Data::Keywords;
using Poco::Data::Session;
using Poco::Data::Statement;

DBPersister::DBPersister(ConfigurationParams& configuration)
   : _databaseName(configuration.sqlConfig.databaseName)
   , _configurationParams(configuration)
{
}

DBPersister::~DBPersister() {}

void DBPersister::initSession()
{
   if(!_db_session)
   {
      std::string dbConnString;
      dbConnString = "host=" + _configurationParams.sqlConfig.host;
      dbConnString += ";user=" + _configurationParams.sqlConfig.user;
      dbConnString += ";password=" + _configurationParams.sqlConfig.password;
      dbConnString += ";compress=true;auto-reconnect=true";

      _db_session =
         std::make_unique<Session>(Poco::Data::SessionFactory::instance().create(
            Poco::Data::MySQL::Connector::KEY, dbConnString));
   }
}

void DBPersister::operator()(const Contract& contract)
{
   try
   {
      initSession();
      if(contract.getDate().getDay() == -1)
      {
         // This is a vacancy
         persistVacancy(contract);
      }
      else
      {
         persistContract(contract);
      }
   }
   catch(Poco::Data::ConnectionFailedException& e)
   {
      Poco::LogStream logger("Grabber.DBPersister");
      logger.error() << (std::string("Connection Exception: ") +
                         std::string(e.displayText()))
                     << std::endl;
   }
   catch(Poco::Data::MySQL::MySQLException& e)
   {
      Poco::LogStream logger("Grabber.DBPersister");
      logger.error() << (std::string("SQL error: ") + std::string(e.displayText()))
                     << std::endl;
   }
}


void DBPersister::persistVacancy(const Contract& contract)
{
   Poco::LocalDateTime currentDate;
   Poco::Data::Date    pocoCurrentDate(
      currentDate.year(), currentDate.month(), currentDate.day());

   Statement insert(*_db_session);

   int typeOfPersonal = contract.getTypeOfPersonalId();
   int duration       = contract.getDurationId();
   int system         = contract.getSystemId();
   int unit           = contract.getUnit();
   int role           = contract.getRole();

   insert << "insert into " + _databaseName +
                ".vacancies(type_of_personal_id,"
                "unit_id,"
                "role_id,"
                "duration_id,"
                "system_id,"
                "last_check,"
                "num_checks)"
                " values (?,?,?,?,?,?,1)"
                " on duplicate key update "
                " last_check=?,"
                " num_checks=num_checks+1",
      use(typeOfPersonal), use(unit), use(role), use(duration), use(system),
      use(pocoCurrentDate), use(pocoCurrentDate);
   insert.execute();
}

void DBPersister::persistContract(const Contract& contract)
{
   Contract::Date   date = contract.getDate();
   Poco::Data::Date pocoDate(date.getYear(), date.getMonth(), date.getDay());

   Poco::LocalDateTime currentDate;
   Poco::Data::Date    pocoCurrentDate(
      currentDate.year(), currentDate.month(), currentDate.day());

   Statement insert(*_db_session);

   int    typeOfPersonal = contract.getTypeOfPersonalId();
   int    duration       = contract.getDurationId();
   int    system         = contract.getSystemId();
   int    unit           = contract.getUnit();
   int    role           = contract.getRole();
   double mark           = contract.getMark();

   insert << "insert into " + _databaseName +
                ".contracts(type_of_personal_id,"
                "unit_id,"
                "role_id,"
                "duration_id,"
                "system_id,"
                "mark,"
                "date,"
                "last_check,"
                "num_checks)"
                " values (?,?,?,?,?,?,?,?,1)"
                " on duplicate key update"
                " last_check=?,"
                " num_checks=num_checks+1",
      use(typeOfPersonal), use(unit), use(role), use(duration), use(system), use(mark),
      use(pocoDate), use(pocoCurrentDate), use(pocoCurrentDate);
   insert.execute();
}
