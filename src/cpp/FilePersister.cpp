#include "FilePersister.h"
#include "Contract.h"

FilePersister::FilePersister(const std::string& filePath) : Persister()
{
   _output.open(filePath, std::ofstream::out | std::ofstream::app);
}

FilePersister::~FilePersister()
{
   _output.close();
}

void FilePersister::operator()(const Contract& contract)
{
   _output << contract.getTypeOfPersonal() << ";" << contract.getUnit() << ";"
           << contract.getRole() << ";" << contract.getDuration() << ";"
           << contract.getSystem() << ";" << contract.getMark() << ";"
           << contract.getDate().getDay() << "-" << contract.getDate().getMonth() << "-"
           << contract.getDate().getYear() << std::endl;
}
