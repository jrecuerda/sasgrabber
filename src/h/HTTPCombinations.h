#pragma once

#include <memory>
#include <vector>

class HTTPFormCombination
{
public:
   HTTPFormCombination(int unit, int role, char typeOfPersonal);
   ~HTTPFormCombination();
   HTTPFormCombination(HTTPFormCombination&)  = default;
   HTTPFormCombination(HTTPFormCombination&&) = default;

   int  getUnit() const;
   int  getRole() const;
   char getTypeOfPersonal() const;

private:
   int  _unit;
   int  _role;
   char _typeOfPersonal;
};

class HTTPCombinations
{
public:
   HTTPCombinations()                   = default;
   HTTPCombinations(HTTPCombinations&)  = default;
   HTTPCombinations(HTTPCombinations&&) = default;


   const std::vector<HTTPFormCombination>& getFormCombinations();

private:
   void setUpCombinations(std::vector<HTTPFormCombination>& combinations);
};
