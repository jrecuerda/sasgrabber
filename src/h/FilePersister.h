#pragma once

#include "Persister.h"

#include <fstream>

class Contract;

class FilePersister : public Persister
{
public:
   FilePersister(const std::string& filePath);
   virtual ~FilePersister();
   virtual void operator()(const Contract& contract) override;

private:
   std::ofstream _output;
};
