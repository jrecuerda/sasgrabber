#pragma once

#include <memory>
#include <mutex>
#include <string>

namespace Poco
{
namespace Net
{
class HTMLForm;
class HTTPRequest;
class HTTPSClientSession;
} // namespace Net
} // namespace Poco

class RequestorSAS
{
public:
   RequestorSAS(const std::string& uriStr);
   RequestorSAS(RequestorSAS&&) = default;

   std::string requestForm(Poco::Net::HTMLForm& form);

private:
   std::shared_ptr<Poco::Net::HTTPRequest>        _request;
   std::shared_ptr<Poco::Net::HTTPSClientSession> _session;
};
