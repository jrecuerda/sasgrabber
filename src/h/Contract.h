#pragma once

class Contract
{
public:
   class Date
   {
   public:
      Date();
      Date(int day, int month, int year);
      Date& operator=(const Date&) = default;

      int  getDay() const;
      void setDay(int day);

      int  getMonth() const;
      void setMonth(int month);

      int  getYear() const;
      void setYear(int year);

   private:
      int _day;
      int _month;
      int _year;
   };


   Contract(int unit, int role, char typeOfPersonal, char duration, char system);

   int  getUnit() const;
   int  getRole() const;
   char getTypeOfPersonal() const;
   int  getTypeOfPersonalId() const;

   char getDuration() const;
   int  getDurationId() const;

   char getSystem() const;
   int  getSystemId() const;

   static char getTypeOfPersonal(int type);
   static int  getTypeOfPersonal(char type);
   static char getDuration(int duration);
   static int  getDuration(char duration);
   static char getSystem(int sytem);
   static int  getSystem(char system);

   double getMark() const;
   void   setMark(double mark);

   Date getDate() const;
   void setDate(Date date);

private:
   int  _unit;
   int  _role;
   char _typeOfPersonal;
   char _duration;
   char _system;

   double _mark;
   Date   _date;
};
