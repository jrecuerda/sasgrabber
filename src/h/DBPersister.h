#pragma once

#include "Persister.h"

#include <memory>

class Contract;
class ConfigurationParams;

namespace Poco
{
namespace Data
{
class Session;
}
} // namespace Poco

class DBPersister : public Persister
{
public:
   DBPersister(ConfigurationParams& configuration);
   virtual ~DBPersister();
   virtual void operator()(const Contract& contract) override;

private:
   void initSession();
   void persistVacancy(const Contract& contract);
   void persistContract(const Contract& contract);

   ConfigurationParams&                 _configurationParams;
   std::unique_ptr<Poco::Data::Session> _db_session;
   std::string                          _databaseName;
};
