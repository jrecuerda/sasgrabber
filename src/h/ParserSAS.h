#pragma once

#include "Contract.h"

#include <regex>
#include <string>

class NotMatchedException : public std::exception
{
public:
   /** Constructor (C strings).
    *  @param message C-style string error message.
    *                 The string contents are copied upon construction.
    *                 Hence, responsibility for deleting the char* lies
    *                 with the caller.
    */
   explicit NotMatchedException(const char* message) : msg_(message) {}

   /** Constructor (C++ STL strings).
    *  @param message The error message.
    */
   explicit NotMatchedException(const std::string& message) : msg_(message) {}

   /** Destructor.
    * Virtual to allow for subclassing.
    */
   virtual ~NotMatchedException() throw() {}

   /** Returns a pointer to the (constant) error description.
    *  @return A pointer to a const char*. The underlying memory
    *          is in posession of the Exception object. Callers must
    *          not attempt to free the memory.
    */
   virtual const char* what() const throw()
   {
      return msg_.c_str();
   }

protected:
   /** Error message.
    */
   std::string msg_;
};

class ParserSAS
{
public:
   bool operator()(Contract& contract, const std::string& response);

private:
   bool        nobodyFound(const std::string& response) const;
   std::string getLabelNotPared(const std::string& response) const;
};
