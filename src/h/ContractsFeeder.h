#pragma once

#include "ConfigurationParams.h"
#include "Contract.h"

#include <memory>
#include <mutex>
#include <vector>

class HTTPFormCombination;
namespace Poco
{
namespace Data
{
class Session;
}
} // namespace Poco

class ContractsFeeder
{
public:
   ContractsFeeder(ConfigurationParams& configuration);
   ~ContractsFeeder();
   Contract& operator()();

private:
   std::mutex                      _contractsMtx;
   std::vector<Contract>           _contracts;
   std::vector<Contract>::iterator _currIt;
   ConfigurationParams&            _configuration;

   int _combinationsParsed;
   int _contractsParsed;
   int _vacanciesParsed;
   int _requestsDone;
   int _requestsLogCicles;

   void recreateContracts();
   void fillContracts();
   void fillWithCombinations();
   void fillWithContracts();
   void fillWithVacancies();
   void shuffle();

   void                                 initSession();
   std::shared_ptr<Poco::Data::Session> _db_session;
   std::string                          _databaseName;

   std::vector<Contract>
      getContractsFromCombination(const HTTPFormCombination& combination);
};
