#pragma once
#include <fstream>

class Contract;

class Persister
{
public:
   Persister();
   virtual ~Persister();
   virtual void operator()(const Contract& contract) = 0;

private:
};
