#pragma once

#include <string>

namespace Poco
{
namespace Util
{
class AbstractConfiguration;
}
} // namespace Poco

struct SQLConfiguration
{
   SQLConfiguration(Poco::Util::AbstractConfiguration* configuration);

   std::string outputFile;
   std::string host;
   std::string user;
   std::string password;
   std::string databaseName;
};

struct ConfigurationParams
{
   ConfigurationParams(Poco::Util::AbstractConfiguration* configuration);

   bool             filePersister;
   int              numWorkers;
   std::string      inputputFile;
   std::string      outputFile;
   std::string      url;
   bool             beginWithCombinations;
   int              contractsExecPerVacancies;
   SQLConfiguration sqlConfig;
};
