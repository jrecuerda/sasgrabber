#pragma once

#include "Contract.h"
#include "ParserSAS.h"
#include "RequestorSAS.h"

#include <atomic>
#include <memory>
#include <vector>

class HTTPFormCombination;
class ConfigurationParams;
class Persister;
class ContractsFeeder;

#include <Poco/Runnable.h>

namespace Poco
{
namespace Net
{
class HTMLForm;
}
} // namespace Poco

class ExecutorSAS : public Poco::Runnable
{
public:
   ExecutorSAS(
      ConfigurationParams& configuration,
      ContractsFeeder&     contractsFeeder,
      Persister&           persister);

   ~ExecutorSAS();

   virtual void run();

   void stopExecution();

private:
   std::atomic_bool     _stopExecution;
   ContractsFeeder&     _contractsFeeder;
   RequestorSAS         _requestor;
   ParserSAS            _parser;
   Persister&           _persister;
   ConfigurationParams& _configurationParams;

   void createHTMLForm(const Contract& contract, Poco::Net::HTMLForm& form);
   void execute(Contract& contract);
   bool makeRequest(Contract& contract);
};
