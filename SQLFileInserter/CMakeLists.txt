cmake_minimum_required (VERSION 2.8)
project (SQLFileInserter)

 set(CPP_FILES_SQLFileInserter
	  src/cpp/main.cpp)

add_executable(SQLFileInserter ${CPP_FILES_SQLFileInserter})
target_link_libraries(SQLFileInserter ${CONAN_LIBS})
