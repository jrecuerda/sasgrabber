#include <Poco/Util/XMLConfiguration.h>
#include <Poco/Exception.h>
#include <Poco/Data/MySQL/MySQLException.h>
#include <Poco/Data/MySQL/Connector.h>
#include <Poco/Data/SessionFactory.h>

#include <iostream>
#include <fstream>
#include <memory>
#include <regex>
#include <tuple>

using namespace Poco::Util;
using namespace Poco::Data::Keywords;
using Poco::Data::Session;
using Poco::Data::Statement;

struct SQLConfiguration
{
	SQLConfiguration(AbstractConfiguration* configuration)
	{
		outputFile = configuration->getString("OutputFile");
		host = configuration->getString("SQLConfig[@id=host]");
		user = configuration->getString("SQLConfig[@id=user]");
		password = configuration->getString("SQLConfig[@id=password]");
		databaseName = configuration->getString("SQLConfig[@id=database_name]");
	}

	std::string outputFile;
	std::string host;
	std::string user;
	std::string password;
	std::string databaseName;
};

struct ConfigurationParams
{
	ConfigurationParams(AbstractConfiguration* configuration)
		: sqlConfig(configuration)
	{
		inputputFile = configuration->getString("InputFile");
	}

	std::string inputputFile;
	SQLConfiguration sqlConfig;
};

struct Contract
{
	Contract()
		: role(-1)
		, unit(-1)
		, system(-1)
		, duration(-1)
		, typeOfPersonal(-1)
		, mark(-1.0)
		, day(-1)
		, month(-1)
		, year(-1)
	{
	}

	std::string print() const
	{
		return std::to_string(typeOfPersonal) + ";"
				+ std::to_string(unit) + ";"
				+ std::to_string(role) + ";"
				+ std::to_string(duration) + ";"
				+ std::to_string(system) + ";"
				+ std::to_string(mark) + ";"
				+ std::to_string(day) + "-"
				+ std::to_string(month) + "-"
				+ std::to_string(year) + ";";

	}

	int role;
	int unit;
	int system;
	int duration;
	int typeOfPersonal;
	double mark;
	int day;
	int month;
	int year;
};

int processInt(const std::string& input)
{
	return atoi(input.c_str());
}
double processDouble(const std::string& input)
{
	return atof(input.c_str());
}
int processTypeOfPersonal(const std::string& input)
{
	if(input == "A")
	{
		return 1;
	}
	else if(input == "B")
	{
		return 2;
	}
	else if(input == "C")
	{
		return 3;
	}
	else
	{
		//JLOG4CXX_ERROR(logger, "TypeOfPersonal:" + input + "; Not Valid");
		throw;
	}
}
int processDuration(const std::string& input)
{
	if(input == "S")
	{
		return 1;
	}
	else if(input == "V")
	{
		return 2;
	}
	else
	{
		//JLOG4CXX_ERROR(logger, "Duration:" + input + "; Not Valid");
		throw;
	}

}
int processSystem(const std::string& input)
{
	if(input == "L")
	{
		return 1;
	}
	else if(input == "R")
	{
		return 2;
	}
	else if(input == "I")
	{
		return 3;
	}
	else
	{
		//JLOG4CXX_ERROR(logger, "System:" + input + "; Not Valid");
		throw;
	}
}

std::tuple<int,int,int> processDate(const std::string& input)
{
	std::size_t found = input.find("--");
	if(found == std::string::npos)
	{

		std::regex rgx ("([0-9]*)-([0-9]*)-([0-9]*)");
		std::smatch matches;
		if(std::regex_search(input, matches, rgx))
		{
			return std::make_tuple(atoi(matches[1].str().c_str()),atoi(matches[2].str().c_str()),atoi(matches[3].str().c_str()));
		}
	}
	return std::make_tuple(-1,-1,-1);
}

Contract processLine(std::string& line)
{
	Contract contract;
	std::regex rgx("(.*);(.*);(.*);(.*);(.*);(.*);(.*)");
	std::smatch matches;
	if(std::regex_search(line, matches, rgx))
	{
		contract.typeOfPersonal = processTypeOfPersonal(matches[1].str());
		contract.unit = processInt(matches[2].str());
		contract.role = processInt(matches[3].str());
		contract.duration = processDuration(matches[4].str());
		contract.system = processSystem(matches[5].str());
		contract.mark = processDouble(matches[6].str());
		auto date = processDate(matches[7].str());
		contract.day = std::get<0>(date);
		contract.month = std::get<1>(date);
		contract.year = std::get<2>(date);
	}
	return contract;
}

class SQLPerister
{
public:
	SQLPerister(ConfigurationParams &configurationParams)
	{
		Poco::Data::MySQL::Connector::registerConnector();

		std::string dbConnString;
		dbConnString =  "host=" + configurationParams.sqlConfig.host;
		dbConnString += ";user=" + configurationParams.sqlConfig.user;
		dbConnString += ";password=" + configurationParams.sqlConfig.password;
		dbConnString += ";compress=true;auto-reconnect=true";
		_db_session = std::make_shared<Session>(Poco::Data::SessionFactory::instance().create(Poco::Data::MySQL::Connector::KEY, dbConnString ));
		_databaseName = configurationParams.sqlConfig.databaseName;

	}

	void persistVacancy(Contract& contract)
	{
		Poco::LocalDateTime currentDate;
		Poco::Data::Date pocoCurrentDate(currentDate.year(),currentDate.month(),currentDate.day());

		Statement insert(*_db_session);
		int typeOfPersonal = contract.typeOfPersonal;
		int duration = contract.duration;
		int system = contract.system;
		int unit = contract.unit;
		int role = contract.role;
		insert << "insert into " + _databaseName + ".vacancies(type_of_personal_id,"
																 "unit_id,"
																 "role_id,"
																 "duration_id,"
																 "system_id,"
																 "last_check,"
																 "num_checks)"
																 " values (?,?,?,?,?,?,1)"
																 " on duplicate key update "
																 " last_check=?,"
																 " num_checks=num_checks+1",
				use(typeOfPersonal),
				use(unit),
				use(role),
				use(duration),
				use(system),
				use(pocoCurrentDate),
				use(pocoCurrentDate);
		insert.execute();
	}

	void persistContract(Contract& contract)
	{
		Poco::Data::Date pocoDate(contract.year,contract.month,contract.day);

		Poco::LocalDateTime currentDate;
		Poco::Data::Date pocoCurrentDate(currentDate.year(),currentDate.month(),currentDate.day());

		Statement insert(*_db_session);
		int typeOfPersonal = contract.typeOfPersonal;
		int duration = contract.duration;
		int system = contract.system;
		int unit = contract.unit;
		int role = contract.role;
		double mark = contract.mark;
		insert << "insert into " + _databaseName + ".contracts(type_of_personal_id,"
																 "unit_id,"
																 "role_id,"
																 "duration_id,"
																 "system_id,"
																 "mark,"
																 "date,"
																 "last_check,"
																 "num_checks)"
																 " values (?,?,?,?,?,?,?,?,1)"
																 " on duplicate key update"
																 " last_check=?,"
																 " num_checks=num_checks+1",
				use(typeOfPersonal),
				use(unit),
				use(role),
				use(duration),
				use(system),
				use(mark),
				use(pocoDate),
				use(pocoCurrentDate),
				use(pocoCurrentDate);
		insert.execute();
	}

	void operator()(Contract contract)
	{
		try
		{
			if(contract.day == -1)
			{
				// This is a vacancy
				persistVacancy(contract);
			}
			else
			{
				persistContract(contract);
			}
		}
		catch(Poco::Data::MySQL::MySQLException& e)
		{
			//JLOG4CXX_ERROR(logger, std::string("SQL error: ") + contract.print() + std::string(e.displayText()));
		}
	}

private:
	std::shared_ptr<Session> _db_session;
	std::string _databaseName;
	std::string _vacancyInsert;
	std::string _contractInsert;

};

void execute(ConfigurationParams& configurationParams)
{
	std::ifstream fileIn(configurationParams.inputputFile);
	if(!fileIn.is_open())
	{
		//JLOG4CXX_ERROR(logger, "Input file:" + configurationParams.inputputFile + "; Could not be opened");
	}
	SQLPerister persister(configurationParams);

	std::string line;
	while(std::getline(fileIn, line))
	{
		Contract contract = processLine(line);
		persister(contract);
	}

}

int main(int argc, char* argv[])
{
	int result = EXIT_SUCCESS;
	try
	{
		// Set up a simple configuration that logs on the console.
		BasicConfigurator::configure();

		// No need to be deleted
		AbstractConfiguration *poco_configuration = new XMLConfiguration(CONFIGURATION_FILE);

		//JLOG4CXX_INFO(logger, "Entering application.");
		ConfigurationParams configurationParams(poco_configuration);
		execute(configurationParams);
		//JLOG4CXX_INFO(logger, "Exiting application.");
	}
	catch(Poco::NotFoundException& e)
	{
		result = EXIT_FAILURE;
		//JLOG4CXX_ERROR(logger, std::string("Configuration parameter not found: ") + std::string(e.what()));
	}
	catch(Poco::FileNotFoundException& e)
	{
		result = EXIT_FAILURE;
		//JLOG4CXX_ERROR(logger, std::string("Configuration file \"configuration.xml\" not found: ") + std::string(e.what()));
	}
	catch(Poco::Exception& e)
	{
		result = EXIT_FAILURE;
		//JLOG4CXX_ERROR(logger, std::string("Unexpected error:") + std::string(e.what()));
	}
	catch(...)
	{
		//JLOG4CXX_ERROR(logger, "Unexpected error occurred :-(");
		result = EXIT_FAILURE;
	}

	return result;
}
