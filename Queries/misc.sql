select count(*) CONTRACTS from bolsatrabajofirst.contracts;
select count(*) VACANCIES from bolsatrabajofirst.vacancies;

# General Select
SELECT
rol.name Rol, uni.name Unidad, c.mark Nota, typeOf.name Tipo, dur.name Duracion, sys.name Sistema
FROM
contracts c,
type_of_personal typeOf,
duration dur,
system sys,
role rol,
unit uni
WHERE
typeOf.id = c.type_of_personal_id
AND dur.id = c.duration_id
AND sys.id = c.system_id
AND rol.id = c.role_id
AND uni.id = c.unit_id
AND rol.name like 'ENFERMERA'
and uni.name like '%MOTRIL%'
ORDER BY typeOf.name , dur.name , sys.name , rol.name , uni.name;

# General Select order by date
SELECT
c.date Fecha, rol.name Rol, uni.name Unidad, c.mark Nota, typeOf.name Tipo, dur.name Duracion, sys.name Sistema
FROM
contracts c,
type_of_personal typeOf,
duration dur,
system sys,
role rol,
unit uni
WHERE
typeOf.id = c.type_of_personal_id
AND dur.id = c.duration_id
AND sys.id = c.system_id
AND rol.id = c.role_id
AND uni.id = c.unit_id
#AND rol.name like '%FISIO%'
#and uni.name like '%MOTRIL%'
ORDER BY Fecha DESC;

# General Select order by date
SELECT
count(*) UPDATES, c.date Fecha, rol.name Rol, uni.name Unidad, c.mark Nota, typeOf.name Tipo, dur.name Duracion, sys.name Sistema
FROM
contracts c,
type_of_personal typeOf,
duration dur,
system sys,
role rol,
unit uni
WHERE
typeOf.id = c.type_of_personal_id
AND dur.id = c.duration_id
AND sys.id = c.system_id
AND rol.id = c.role_id
AND uni.id = c.unit_id
#AND rol.name like '%FISIO%'
#and uni.name like '%MOTRIL%'
group by Rol, Unidad, Tipo, Duracion, Sistema, Fecha
ORDER BY Updates DESC;

SELECT
AVG(c.mark), rol.name ROL
FROM
contracts c,
type_of_personal typeOf,
duration dur,
system sys,
role rol,
unit uni
WHERE
typeOf.id = c.type_of_personal_id
AND dur.id = c.duration_id
AND sys.id = c.system_id
AND rol.id = c.role_id
AND uni.id = c.unit_id
AND rol.name like '%ENFERMERA%'
and c.duration_id = 1
and c.system_id = 1
group by c.role_id;